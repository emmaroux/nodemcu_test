/*
  Ayé il est sur gitlab ! 
  ESP8266 Blink by Simon Peter
  Blink the blue LED on the ESP-01 module
  This example code is in the public domain

  The blue LED on the ESP-01 module is connected to GPIO1
  (which is also the TXD pin; so we cannot use Serial.print() at the same time)

  Note that this sketch uses LED_BUILTIN to find the pin with the internal LED
*/

int fsrAnalogPin = 0; // FSR branché sur pin Analog 0
  int LEDpin = D4; // connecter LED rouge sur pin 11 (pin PWM)
  int fsrReading;  // Lecture analogique de la tension du pont 
                   //    diviseur FSR + Resistance Pull-Down
  int LEDbrightness;

void setup() {
  Serial.begin(9600); // Envoi de message de déboggage sur connexion série
  pinMode(LEDpin, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
}

// the loop function runs over and over again forever
void loop() {

fsrReading = analogRead(fsrAnalogPin);
    Serial.print("Analog reading = ");
    Serial.println(fsrReading);
    // Nous devons convertir la valeur analogique lue (0-1023) 
    // en une valeur utilisable par analogWrite (0-255).
    // C'est ce que fait l'instruction map!
    LEDbrightness = map(fsrReading, 0, 1023, 0, 255);
    // LED gets brighter the harder you press
    analogWrite(LEDpin, LEDbrightness);
    delay(100);}
